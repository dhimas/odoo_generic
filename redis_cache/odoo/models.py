import logging

from odoo import api, _
from odoo.exceptions import MissingError, AccessError
from odoo.models import BaseModel, MetaModel
from odoo.osv.query import Query
from odoo.tools import pycompat
import datetime
import redis
import pickle

REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_EXPIRED = 60*60*24*1
REDIS_SKIP = ['ir_ui_view', 'ir_ui_menu', 'ir_module_module', 'ir_model', 'ir_act_server', 'ir_act_window', 'ir_act_report_xml', 'ir_rule', 'ir_attachment', 'ir_module_module_dependency', 'ir_module_category', 'res_lang', 'bus_presence', 'mail_channel']

_logger = logging.getLogger(__name__)

old_write = BaseModel.write

@api.multi
def write(self, vals):
    res =  old_write(self, vals)
    cache = False
    try:
        cache = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
        cache.ping()
    except redis.ConnectionError as e:
        _logger.warning(e)
        cache = False
    if cache and self._table not in REDIS_SKIP and self.ids:
        for obj_id in self.ids:
            cache.delete("%s_%s_%s" % (self._cr.dbname, self._table, obj_id))
    return res

@api.multi
def _read_from_database(self, field_names, inherited_field_names=[]):
    """ Read the given fields of the records in ``self`` from the database,
        and store them in cache. Access errors are also stored in cache.

        :param field_names: list of column names of model ``self``; all those
            fields are guaranteed to be read
        :param inherited_field_names: list of column names from parent
            models; some of those fields may not be read
    """
    if not self:
        return

    env = self.env
    cr, user, context = env.args

    # make a query object for selecting ids, and apply security rules to it
    param_ids = object()
    query = Query(['"%s"' % self._table], ['"%s".id IN %%s' % self._table], [param_ids])
    self._apply_ir_rules(query, 'read')

    # determine the fields that are stored as columns in tables; ignore 'id'
    fields_pre = [
        field
        for field in (self._fields[name] for name in field_names + inherited_field_names)
        if field.name != 'id'
        if field.base_field.store and field.base_field.column_type
        if not (field.inherited and callable(field.base_field.translate))
    ]

    # the query may involve several tables: we need fully-qualified names
    def qualify(field):
        col = field.name
        res = self._inherits_join_calc(self._table, field.name, query)
        if field.type == 'binary' and (context.get('bin_size') or context.get('bin_size_' + col)):
            # PG 9.2 introduces conflicting pg_size_pretty(numeric) -> need ::cast
            res = 'pg_size_pretty(length(%s)::bigint)' % res
        return '%s as "%s"' % (res, col)

    # selected fields are: 'id' followed by fields_pre
    qual_names = [qualify(name) for name in [self._fields['id']] + fields_pre]

    # determine the actual query to execute
    from_clause, where_clause, params = query.get_sql()
    query_str = "SELECT %s FROM %s WHERE %s" % (",".join(qual_names), from_clause, where_clause)

    # fetch one list of record values per field
    field_values_list = [[] for name in qual_names]
    param_pos = params.index(param_ids)
    for sub_ids in cr.split_for_in_conditions(self.ids):
        params[param_pos] = tuple(sub_ids)
        rows = []
        # start caching service
        cache = False
        try:
            cache = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
            cache.ping()
        except redis.ConnectionError as e:
            _logger.warning(e)
            cache = False
        # filtered only for non join table query
        check_in_redis = True if param_pos == 0 and cache and self._table not in REDIS_SKIP else False
        fallback_to_query = True
        missing_sub_ids = []
        if check_in_redis:
            # if needed to check redis
            # find based on id
            for sub_id in sub_ids:
                key = '%s_%s_%s' % (cr.dbname, self._table, sub_id)
                sub_key = ''.join([x.name for x in fields_pre])
                if cache.hexists(key, sub_key):
                    try:
                        rows.append(pickle.loads(cache.hget(key, sub_key)))
                    except Exception as e:
                        _logger.warning('redis cache fail %s' % e)
                        missing_sub_ids.append(sub_id)
                else:
                    missing_sub_ids.append(sub_id)
            if not missing_sub_ids:
                fallback_to_query = False
        # fallback to query if true
        if fallback_to_query:
            if check_in_redis:
                _logger.warning('redis cache fallback %s' % self._table)
            if missing_sub_ids:
                params[param_pos] = tuple(missing_sub_ids)
            cr.execute(query_str, params)
            temp_rows = cr.fetchall()
            if cache:
                for row in temp_rows:
                    key = '%s_%s_%s' % (cr.dbname, self._table, row[0])
                    sub_key = ''.join([x.name for x in fields_pre])
                    try:
                        cache.hset(key, sub_key, pickle.dumps(row))
                        cache.expire(key, REDIS_EXPIRED)
                    except Exception as e:
                        _logger.warning('redis cache fail %s' % e)
            rows += temp_rows
        for row in rows:
            for values, val in pycompat.izip(field_values_list, row):
                values.append(val)

    ids = field_values_list.pop(0)
    fetched = self.browse(ids)

    if ids:
        # translate the fields if necessary
        if context.get('lang'):
            for field, values in pycompat.izip(fields_pre, field_values_list):
                if not field.inherited and callable(field.translate):
                    name = field.name
                    translate = field.get_trans_func(fetched)
                    for index in range(len(ids)):
                        values[index] = translate(ids[index], values[index])

        # store result in cache
        target = self.browse([], self._prefetch)
        for field, values in pycompat.izip(fields_pre, field_values_list):
            convert = field.convert_to_cache
            # Note that the target record passed to convert below is empty.
            # This does not harm in practice, as it is only used in Monetary
            # fields for rounding the value. As the value comes straight
            # from the database, it is expected to be rounded already.
            values = [convert(value, target, validate=False) for value in values]
            self.env.cache.update(fetched, field, values)

        # determine the fields that must be processed now;
        # for the sake of simplicity, we ignore inherited fields
        for name in field_names:
            field = self._fields[name]
            if not field.column_type:
                field.read(fetched)

    # Warn about deprecated fields now that fields_pre and fields_post are computed
    for name in field_names:
        field = self._fields[name]
        if field.deprecated:
            _logger.warning('Field %s is deprecated: %s', field, field.deprecated)

    # store failed values in cache for the records that could not be read
    missing = self - fetched
    if missing:
        extras = fetched - self
        if extras:
            raise AccessError(
                _("Database fetch misses ids ({}) and has extra ids ({}), may be caused by a type incoherence in a previous request").format(
                    missing._ids, extras._ids,
                ))
        # mark non-existing records in missing
        forbidden = missing.exists()
        if forbidden:
            _logger.info(
                _('The requested operation cannot be completed due to record rules: Document type: %s, Operation: %s, Records: %s, User: %s') % \
                (self._name, 'read', ','.join([str(r.id) for r in self][:6]), self._uid))
            # store an access error exception in existing records
            exc = AccessError(
                _('The requested operation cannot be completed due to security restrictions. Please contact your system administrator.\n\n(Document type: %s, Operation: %s)') % \
                (self._name, 'read')
            )
            self.env.cache.set_failed(forbidden, self._fields.values(), exc)

BaseModel.write = write
BaseModel._read_from_database = _read_from_database