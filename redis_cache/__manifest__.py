{
    'name': 'Redis Cache',
    'version': '12.0.1.0.0',
    'summary': 'Redis caching strategy',
    'description': '''
        require redis-py: pip3 install redis
        odoo monkeypatch:
        https://odoo-development.readthedocs.io/en/latest/dev/hooks/post_load.html
    ''',
    'category': 'tools',
    'contributors': [
        'Dhimas Yudangga A',
    ],
    'depends': [
        'base',
    ],
    'external_dependencies': {
        'python': [
            'redis',
        ],
    },
    'post_load': 'post_load',
    'installable': True,
    'auto_install': False,
    'application': False,
}
